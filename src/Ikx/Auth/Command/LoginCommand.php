<?php
namespace Ikx\Auth\Command;

use Ikx\Core\Command\AbstractCommand;
use Ikx\Core\Command\CommandInterface;
use Ikx\Core\Entity\User;
use Ikx\Core\Utils\Format;
use Ikx\Core\Utils\MessagingTrait;

class LoginCommand extends AbstractCommand implements CommandInterface {
    use MessagingTrait;

    public function describe()
    {
        return "Login command";
    }

    public function run()
    {
        if (!isset($this->params[0]) || !isset($this->params[1])) {
            $this->notice($this->nickname, __('%s: %s <username> <password>',
                Format::bold(__('SYNTAX ERROR')), $this->command));
        } else {
            $userCollection = $this->network->getDb()->users;
            $username = $this->params[0];
            $password = $this->params[1];

            $cursor = $userCollection->findOne([
                'username'      => $username
            ]);

            if ($cursor && password_verify($password, $cursor['password'])) {
                /** @var User $user */
                $user = $this->network->getUser($this->nickname);
                if ($user) {
                    $user->setLevel($cursor->level)
                         ->setLoginName($cursor->username);
                    $this->notice($this->nickname, __("You are now logged in as %s", $cursor->username));

                    $this->server->log(__('%s is now logged in as %s with level %d', $this->nickname, $cursor->username, $cursor->level));
                }
            } else {
                $this->notice($this->nickname, __('%s: Invalid login information supplied',
                    Format::bold(__('ERROR'))));

                $this->server->log(__('%s supplied invalid login information', $this->nickname));
            }
        }
    }
}