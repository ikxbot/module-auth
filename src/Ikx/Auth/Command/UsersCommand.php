<?php
namespace Ikx\Auth\Command;

use Ikx\Core\Command\AbstractCommand;
use Ikx\Core\Command\CommandInterface;
use Ikx\Core\Entity\User;
use Ikx\Core\Utils\Format;
use Ikx\Core\Utils\MessagingTrait;
use Ikx\Core\Utils\Table;

class UsersCommand extends AbstractCommand implements CommandInterface {
    use MessagingTrait;

    public function describe()
    {
        return "Users information command";
    }

    public function run()
    {
        if (!$this->user->getIsSecure()) {
            $this->notice($this->nickname, __("You need to be connected on a secure connection in order to use this command."));
            return;
        }

        $command = strtoupper($this->params[0]) ?? null;
        switch($command) {
            case 'ACCESS':
                $username = $this->params[1] ?? null;
                $level = $this->params[2] ?? null;
                if ($username == null || $level == null) {
                    $this->notice($this->nickname, "Syntax: USERS ACCESS <username> <level>");
                } else if ($username == $this->user->getLoginName()) {
                    $this->notice($this->nickname, "You can't update your own access level!");
                } else if(!is_numeric($level)) {
                    $this->notice($this->nickname, "The level to set must be a valid number.");
                } else if($level >= $this->user->getLevel()) {
                    $this->notice($this->nickname, "You can't set an access level higher than your own level!");
                } else if($level >= 1000) {
                    $this->notice($this->nickname, "Access levels range from 0-999");
                } else {
                    $userCollection = $this->network->getDb()->users;

                    $cursor = $userCollection->findOne([
                        'username'      => $username
                    ]);

                    if (!$cursor) {
                        $this->notice($this->nickname, __("Could not find a user with the username: %s", Format::bold($username)));
                    } else {
                        $userCollection->updateOne(['username' => $username], ['$set' => ['level' => $level]]);
                        $this->notice($this->nickname, __("User level for %s has been changed to %s", $username, Format::bold($level)));
                        $this->notice($this->nickname, __("Please note: changed user levels will only be applied when a user logs in. If the user is already logged, he/she can use the LOGIN command again to update the level."));
                    }
                }
                break;
            case 'RESETPASS':
                $username = $this->params[1] ?? null;
                if ($username == null) {
                    $this->notice($this->nickname, "Syntax: USERS RESETPASS <username>");
                } else {
                    $userCollection = $this->network->getDb()->users;

                    $cursor = $userCollection->findOne([
                        'username'      => $username
                    ]);

                    if (!$cursor) {
                        $this->notice($this->nickname, __("Could not find a user with the username: %s", Format::bold($username)));
                    } else {
                        $pw = $this->generatePassword(rand(8, 15));
                        $pwHash = password_hash($pw, PASSWORD_BCRYPT, ['cost' => 12]);

                        $userCollection->updateOne(['username' => $username], ['$set' => ['password' => $pwHash]]);
                        $this->notice($this->nickname, __("Password for %s has been set to %s", $username, Format::bold($pw)));
                    }
                }
                break;

            case 'LIST':
                $table = new Table();
                $table->addRow(Format::bold(__("Username")), Format::bold("Level"));

                $userCollection = $this->network->getDb()->users;
                $users = $userCollection->find();
                foreach($users as $user) {
                    $table->addRow($user['username'], $user['level']);
                }

                $rows = $table->get();
                foreach ($rows as $row) {
                    $this->notice($this->nickname, $row);
                }

                break;

            case 'HELP':
            default:
                $this->notice($this->nickname, Format::bold(__("USERS command help")));
                $this->notice($this->nickname, __("The USERS command allows to manage registered users."));

                $table = new Table();
                $table->addRow(Format::bold("ACCESS"), __("Alter a user's access level"));
                $table->addRow(Format::bold("LIST"), __("Display a list of registered users"));
                $table->addRow(Format::bold("RESETPASS"), __("Resets a user's password (the \x02new\x02 password will be sent to you)"));

                $rows = $table->get();
                foreach($rows as $row) {
                    $this->notice($this->nickname, $row);
                }
        }
    }

    private function generatePassword($length = 10) {
        $allowedChars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789/*-+~`!@#$%^&*()_=[]{}:;|\<,>./?';
        $password = '';
        for($i = 0; $i < $length; $i++) {
            $r = mt_rand(0, mb_strlen($allowedChars) - 1);
            $password .= $allowedChars[$r];
        }

        return $password;
    }
}