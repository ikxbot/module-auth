<?php
namespace Ikx\Auth\Command;

use Ikx\Core\Command\AbstractCommand;
use Ikx\Core\Command\CommandInterface;
use Ikx\Core\Entity\User;
use Ikx\Core\Utils\Format;
use Ikx\Core\Utils\MessagingTrait;

class WhoamiCommand extends AbstractCommand implements CommandInterface {
    use MessagingTrait;

    public function describe()
    {
        return __("User information command (only displays own information)");
    }

    public function run()
    {
        /** @var User $user */
        $user = $this->network->getUser($this->nickname);
        $this->msg($this->channel, __("You are %s!%s@%s (%s), logged in as %s with level %s",
            $user->getNickname(), $user->getUsername(), $user->getAddress(), $user->getGecos(),
            $user->getLoginName(), $user->getLevel()));

        if ($user->ison($this->channel)) {
            $this->msg($this->channel, __("User has level %d on this channel",
                $user->getChannelLevel($this->channel)));
        }

        if ($user->getVersion() != '') {
            $this->msg($this->channel, __('%s is using %s',
                $user->getNickname(), $user->getVersion()));
        }

        $this->msg($this->channel, __('%s nick ison %s', $user->getNickname(), implode(', ', $user->getChannelNamesWithLevel())));
    }
}