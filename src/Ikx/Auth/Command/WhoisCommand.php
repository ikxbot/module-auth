<?php
namespace Ikx\Auth\Command;

use Ikx\Core\Command\AbstractCommand;
use Ikx\Core\Command\CommandInterface;
use Ikx\Core\Entity\User;
use Ikx\Core\Utils\Format;
use Ikx\Core\Utils\MessagingTrait;

class WhoisCommand extends AbstractCommand implements CommandInterface {
    use MessagingTrait;

    public function describe()
    {
        return __("User information command");
    }

    public function run()
    {
        if (count($this->params)) {
            /** @var User $user */
            $user = $this->network->getUser($this->params[0]);
            if ($user) {
                $this->msg($this->channel, __("%s is %s!%s@%s (%s), logged in as %s with level %s",
                    $this->params[0], $user->getNickname(), $user->getUsername(), $user->getAddress(), $user->getGecos(),
                    $user->getLoginName(), $user->getLevel()));

                if ($user->ison($this->channel)) {
                    $this->msg($this->channel, __("User has level %d on this channel",
                        $user->getChannelLevel($this->channel)));
                }

                if ($user->getVersion() != '') {
                    $this->msg($this->channel, __('%s is using %s',
                        $user->getNickname(), $user->getVersion()));
                }

                $this->msg($this->channel, sprintf('%s nick ison %s', $user->getNickname(), implode(', ', $user->getChannelNamesWithLevel())));
            } else {
                $this->msg($this->channel, __("%s: Unknown user",
                    Format::bold($this->params[0])));
            }
        } else {
            $this->msg($this->channel, __('%s: %s <nickname>',
                Format::bold(__('SYNTAX ERROR')), $this->command));
        }
    }
}