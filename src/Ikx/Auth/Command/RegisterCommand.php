<?php
namespace Ikx\Auth\Command;

use Ikx\Core\Command\AbstractCommand;
use Ikx\Core\Command\CommandInterface;
use Ikx\Core\Entity\User;
use Ikx\Core\Utils\Format;
use Ikx\Core\Utils\MessagingTrait;

class RegisterCommand extends AbstractCommand implements CommandInterface {
    use MessagingTrait;

    public function describe()
    {
        return "Login command";
    }

    public function run()
    {
        if (!isset($this->params[0]) || !isset($this->params[1])) {
            $this->notice($this->nickname, __('%s: %s <username> <password>',
                Format::bold(__('SYNTAX ERROR')), $this->command));
        } else {
            $userCollection = $this->network->getDb()->users;
            $username = $this->params[0];
            $password = $this->params[1];

            $cursor = $userCollection->findOne([
                'username'      => $username
            ]);

            if ($cursor) {
                $this->notice($this->nickname, __('%s: A user with this username already exists',
                    Format::Bold(__('ERROR'))));
            } else {
                $level = User::LEVEL_LOGGEDIN;
                if ($userCollection->count() == 0) {
                    $level = User::LEVEL_BOTOWNER;
                }

                $userCollection->insertOne([
                    'username'          => $username,
                    'password'          => password_hash($password, PASSWORD_BCRYPT),
                    'level'             => $level
                ]);

                $this->notice($this->nickname, __('%s: You have been registered in the bot with level %d',
                    Format::color(Format::bold(__('SUCCESS')), 3), $level));

                if ($level == 1000) {
                    $this->notice($this->nickname, __('You are now an %s of this bot! Please be careful',
                        Format::color(__('owner'), 4)));
                }

                $this->server->log(__('%s has registered as %s with level %d', $this->nickname, $username, $level));
            }
        }
    }
}